/******************************************************************************

   @file  cw.c

   @brief   This file contains the implementation of cw.

 ******************************************************************************/

/*********************************************************************
 * INCLUDES
 */
#include <string.h>

//#include <xdc/runtime/Log.h> // Comment this in to use xdc.runtime.Log
#include <ti/common/cc26xx/uartlog/UartLog.h>  // Comment out if using xdc Log

#include <icall.h>
#include <services/cw.h>

/* This Header file contains all BLE API and icall structure definition */
#include "icall_ble_api.h"


/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * CONSTANTS
 */

/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * GLOBAL VARIABLES
 */

/*********************************************************************
 * LOCAL VARIABLES
 */
volatile bool eh_test = false;

/*********************************************************************
 * Profile Attributes - variables
 */

/*********************************************************************
 * Profile Attributes - Table
 */


/*********************************************************************
 * LOCAL FUNCTIONS
 */
void cw_init()
{
#if 0
    /* Configure the button pin */
    GPIO_setConfig(Board_GPIO_BUTTON1, GPIO_CFG_IN_PU | GPIO_CFG_IN_INT_FALLING);

    /* install Button callback and enable it */
    GPIO_setCallback(Board_GPIO_BUTTON1, ehFxn);
    GPIO_enableInt(Board_GPIO_BUTTON1);
#endif
}

/*********************************************************************
 * PROFILE CALLBACKS
 */
// Simple Profile Service Callbacks

/*********************************************************************
 * PUBLIC FUNCTIONS
 */
/*
 *  ======== ehFxn ========
 */
void ehFxn()
{
    if (eh_test == false)
    {
        eh_test = true;
    } else {
        eh_test = false;
    }
}
