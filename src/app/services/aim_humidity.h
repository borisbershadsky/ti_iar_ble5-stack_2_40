#ifndef AIM_HUMIDITY_H_
#define AIM_HUMIDITY_H_

#include <stdint.h>
#include <stdbool.h>

#define HUMIDITY_SLAVE_ADDRESS 	0x40

#define HTU21D_TRIGGER_TEMPERATURE_MEASUREMENT_HM_CMD    (0xE3)  // Hold Master
#define HTU21D_TRIGGER_HUMIDITY_MEASUREMENT_HM_CMD       (0xE5)  // Hold Master
#define HTU21D_TRIGGER_TEMPERATURE_MEASUREMENT_NHM_CMD   (0xF3)  // No Hold Master
#define HTU21D_TRIGGER_HUMIDITY_MEASUREMENT_NHM_CMD      (0xF5)  // No Hold Master
#define HTU21D_WRITE_USER_REGISTER_CMD                   (0xE6)
#define HTU21D_READ_USER_REGISTER_CMD                    (0xE7)
#define HTU21D_SOFT_RESET_CMD                            (0xFE)

#define HTU21D_DEFAULT_USER_REGISTER                     (0x01)  // 12bits Humidity resolution, 14 bits Temp resolution, VDD>2.25V, 3 bits Reserved, Disable On-Chip heater, Disable OTP

//*****************************************************************************
//
// Prototypes for the APIs.
//
//*****************************************************************************
extern uint32_t aimHumidityInit(uint8_t add);
extern void aimHumidityStartMeasurement(uint8_t add);
extern uint16_t aimHumidityReadVal(uint8_t add);
extern bool HumiditySensorDetected;

#endif /* AIM_HUMIDITY_H_ */
