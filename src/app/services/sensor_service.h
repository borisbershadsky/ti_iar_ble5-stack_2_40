/******************************************************************************

   @file  sensor_service.h

   @brief   This file contains the Sensor_Service service definitions and
          prototypes.


   Group: CMCU, LPRF
   Target Device: CC2652

 ******************************************************************************
   
 Copyright (c) 2015-2018, Texas Instruments Incorporated
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:

 *  Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

 *  Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.

 *  Neither the name of Texas Instruments Incorporated nor the names of
    its contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 ******************************************************************************
   Release Name: simplelink_cc26x2_sdk_2_30_00_34
   Release Date: 2018-10-04 14:27:27
 *****************************************************************************/

#ifndef _SENSOR_SERVICE_H_
#define _SENSOR_SERVICE_H_

#ifdef __cplusplus
extern "C"
{
#endif

/*********************************************************************
 * INCLUDES
 */
#include <bcomdef.h>
#include "osal_tasks.h"

/*********************************************************************
 * CONSTANTS
 */
// Service UUID
#define SENSOR_SERVICE_SERV_UUID 0x1140
#define SENSOR_SERVICE_SERV_UUID_BASE128(uuid) 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, \
    0x00, 0xB0, 0x00, 0x40, 0x51, 0x04, LO_UINT16(uuid), HI_UINT16(uuid), 0x00, \
    0xF0

// Temp Characteristic defines
#define DS_TEMP_ID                 0
#define DS_TEMP_UUID               0x1141
#define DS_TEMP_UUID_BASE128(uuid) 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, \
    0xB0, 0x00, 0x40, 0x51, 0x04, LO_UINT16(uuid), HI_UINT16(uuid), 0x00, 0xF0
#define DS_TEMP_LEN                400
#define DS_TEMP_LEN_MIN            0

// Shock Characteristic defines
#define DS_SHOCK_ID                 1
#define DS_SHOCK_UUID               0x1142
#define DS_SHOCK_UUID_BASE128(uuid) 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, \
    0xB0, 0x00, 0x40, 0x51, 0x04, LO_UINT16(uuid), HI_UINT16(uuid), 0x00, 0xF0
#define DS_SHOCK_LEN                400
#define DS_SHOCK_LEN_MIN            0

#define SENSOR_POLL_PERIOD          5000
#define SENSOR_POLL_EVT             0x0001

/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * Profile Callbacks
 */

// Callback when a characteristic value has changed
typedef void (*SensorServiceChange_t)(uint16_t connHandle, uint8_t paramID,
                                    uint16_t len, uint8_t *pValue);

typedef struct
{
    SensorServiceChange_t pfnChangeCb;          // Called when characteristic value changes
    SensorServiceChange_t pfnCfgChangeCb;       // Called when characteristic CCCD changes
} SensorServiceCBs_t;

typedef enum
{
  EH_STATE_RESTART = 0,
  EH_STATE_DT1,
  EH_STATE_DEMOD,
  EH_STATE_FINALIZE
} aimEHState_t;

/*********************************************************************
 * API FUNCTIONS
 */

/*
 * SensorService_AddService- Initializes the SensorService service by registering
 *          GATT attributes with the GATT server.
 *
 *    rspTaskId - The ICall Task Id that should receive responses for Indications.
 */
extern bStatus_t SensorService_AddService(uint8_t rspTaskId);

/*
 * SensorService_RegisterAppCBs - Registers the application callback function.
 *                    Only call this function once.
 *
 *    appCallbacks - pointer to application callbacks.
 */
extern bStatus_t SensorService_RegisterAppCBs(SensorServiceCBs_t *appCallbacks);

/*
 * SensorService_SetParameter - Set a SensorService parameter.
 *
 *    param - Profile parameter ID
 *    len   - length of data to write
 *    value - pointer to data to write.  This is dependent on
 *            the parameter ID and may be cast to the appropriate
 *            data type (example: data type of uint16_t will be cast to
 *            uint16_t pointer).
 */
extern bStatus_t SensorService_SetParameter(uint8_t param,
                                          uint16_t len,
                                          void *value);

/*
 * SensorService_GetParameter - Get a SensorService parameter.
 *
 *    param - Profile parameter ID
 *    len   - pointer to a variable that contains the maximum length that can be written to *value.
              After the call, this value will contain the actual returned length.
 *    value - pointer to data to write.  This is dependent on
 *            the parameter ID and may be cast to the appropriate
 *            data type (example: data type of uint16_t will be cast to
 *            uint16_t pointer).
 */
extern bStatus_t SensorService_GetParameter(uint8_t param,
                                          uint16_t *len,
                                          void *value);

// sensor poll timeout
extern uint16 sensor_service_ProcessEvent(uint8 taskId, uint16 events );
extern void sensor_service_Init(uint8 taskId);

/*********************************************************************
*********************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* _SENSOR_SERVICE_H_ */
