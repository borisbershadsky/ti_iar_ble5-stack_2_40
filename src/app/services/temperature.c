/*
 * Copyright (c) 2016-2017, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== temperature.c ========
 */
#include <stdint.h>
#include <stddef.h>
//#include <unistd.h>

/* Driver Header files */
#include <ti/drivers/GPIO.h>
#include <ti/drivers/I2C.h>

/* Example/Board Header files */
#include "Board.h"
#include "temperature.h"

#define TASKSTACKSIZE       640

#define TMP007_ADDR         0x40
#define TMP007_DIE_TEMP     0x0001  /* Die Temp Result Register */
#define TMP007_OBJ_TEMP     0x0003  /* Object Temp Result Register */

// TMP102 temp board
#define TMP102_ADDR         0x49    /* Address */
#define TMP102_OBJ_TEMP     0x0000  /* Object Temp Result Register */
#define AIM_TMP102_ADDR     0x48    /* Address */

#ifndef Board_TMP_ADDR
///#define Board_TMP_ADDR       TMP007_ADDR
#define Board_TMP_ADDR       TMP006_ADDR
#endif

/*
 *  ======== TMP Slave Address ========
 */
#define TMP006_ADDR     0x41
#define TMP007_ADDR     0x40
#define TMP116_ADDR     0x49

/*
 *  ======== TMP Registers ========
 *  The DIE register is valid for both TMP006 and TMP007
 *  The OBJ register is only valid for TMP007
 */
#define TMP_DIE_TEMP     0x0001  /* Die Temp Result Register */
#define TMP_OBJ_TEMP     0x0003  /* Object Temp Result Register */
#define TMP116_TEMP      0x0000  /* Die Temp Result Register for TMP116 */

/*
 *  The CC32xx LaunchPads contain an onboard TMP006 or TMP116 where you can
 *  get the DIE temperature.
 *  The Sensors BoosterPack contains a TMP007 where you can get
 *  DIE (TMP_DIE_TEMP) or OBJECT (TMP_OBJ_TEMP) temperature.
 *  We are using the OBJECT temperature for the TMP007 because it's cool!
 *  If you are on a CC32xx LaunchPad and want to use the Sensors
 *  BoosterPack, please remove the ONBOARD_TMP006 (or ONBOARD_TMP116) define
 *  from the compiler options.
 *
 *  Additionally: no calibration is being done on the TMPxxx device to simplify
 *  the example code.
 */
#if defined(ONBOARD_TMP006)
#define TMP_REGISTER TMP_DIE_TEMP
#define TMP_ADDR     TMP006_ADDR
#elif defined(ONBOARD_TMP116)
#define TMP_REGISTER TMP116_TEMP
#define TMP_ADDR     TMP116_ADDR
#else
//#define TMP_REGISTER TMP_OBJ_TEMP
//#define TMP_ADDR     TMP007_ADDR
#define TMP_REGISTER TMP102_OBJ_TEMP
#define TMP_ADDR     TMP102_ADDR
#endif

/* Temperature written by the temperature thread and read by console thread */
volatile float temperatureC;
volatile float temperatureF;

uint16_t readTemperature(void)
{
//    unsigned int    i;
    uint16_t        temperature = 0x55aa;
    uint8_t         txBuffer[1];
    uint8_t         rxBuffer[2];
    I2C_Handle      i2c;
    I2C_Params      i2cParams;
    I2C_Transaction i2cTransaction;
    bool            status;

    /* Call driver init functions */
    GPIO_init();
    I2C_init();

    /* Configure the LED pin */
    GPIO_setConfig(Board_GPIO_LED0, GPIO_CFG_OUT_STD | GPIO_CFG_OUT_LOW);

    /* Turn on user LED */
    GPIO_write(Board_GPIO_LED0, Board_GPIO_LED_ON);

    /* Create I2C for usage */
    I2C_Params_init(&i2cParams);
    i2cParams.bitRate = I2C_100kHz;   // old value =  I2C_400kHz;
    i2c = I2C_open(Board_I2C_TMP, &i2cParams);
    if (i2c == NULL) {
        temperature = 0x55bb;
    } else {

        /* Point to the T ambient register and read its 2 bytes */
        txBuffer[0] = TMP_REGISTER;
        i2cTransaction.slaveAddress = AIM_TMP102_ADDR;
        i2cTransaction.writeBuf = txBuffer;
        i2cTransaction.writeCount = 1;
        i2cTransaction.readBuf = rxBuffer;
        i2cTransaction.readCount = 2;

        /* Take 20 samples and print them out onto the console */
        status = I2C_transfer(i2c, &i2cTransaction);
        if (status) {
                /* Extract degrees C from the received data; see TMP102 datasheet */
                temperature = (rxBuffer[0] << 6) | (rxBuffer[1] >> 2);

                /*
                 * If the MSB is set '1', then we have a 2's complement
                 * negative value which needs to be sign extended
                 */
                if (rxBuffer[0] & 0x80) {
                    temperature |= 0xF000;
                }
               /*
                * For simplicity, divide the temperature value by 32 to get rid of
                * the decimal precision; see TI's TMP007 datasheet
                */
                temperature /= 32;
        } else {
            temperature = 0x6600 | (uint8_t)status;
        }

        /* Deinitialized I2C */
        I2C_close(i2c);

        /* Turn off user LED */
        GPIO_write(Board_GPIO_LED0, Board_GPIO_LED_OFF);
    }
    return (temperature);
}
