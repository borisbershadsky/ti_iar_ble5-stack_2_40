/**************************************************************************************************
 *                                           INCLUDES
 **************************************************************************************************/
#if 0
#include "aim_humidity.h"
#include "aim_i2c.h"
#include "i2c.h"
#include "aim_led.h"
#include "gpio.h" 
#include "sys_ctrl.h"
#include "cpu.h"
#include "sys_ctrl.h"

/*********************************************************************
 * CONSTANTS
 */
/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * GLOBAL VARIABLES
 */

/*********************************************************************
 * LOCAL FUNCTIONS
 */

/*********************************************************************
 * GLOBAL FUNCTIONS
 */

uint32_t aimHumidityInit(uint8_t add)
{
    uint32_t status = I2C_MASTER_ERR_NONE;
    i2c_write_cmd(add, HTU21D_SOFT_RESET_CMD);   // Caller must wait at least 100ms before accessing the device
    status = I2CMasterErr();
    if (status==I2C_MASTER_ERR_NONE)
    {
        i2c_write_byte(add, HTU21D_WRITE_USER_REGISTER_CMD, HTU21D_DEFAULT_USER_REGISTER);
    }
    return status;
}

void aimHumidityStartMeasurement(uint8_t add)
{
   i2c_write_cmd(add, HTU21D_TRIGGER_HUMIDITY_MEASUREMENT_NHM_CMD);      // Caller must wait at least 60ms before reading the measurement
}

uint16_t aimHumidityReadVal(uint8_t add)
{
    return ((i2c_read_word_no_cmd(add)>>4)&0x0FFF);
}
#endif


