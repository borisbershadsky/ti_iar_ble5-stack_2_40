/******************************************************************************

   @file  sensor_service.c

   @brief   This file contains the implementation of the service.

   Group: CMCU, LPRF
   Target Device: CC2652

 ******************************************************************************
   
 Copyright (c) 2015-2018, Texas Instruments Incorporated
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:

 *  Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

 *  Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.

 *  Neither the name of Texas Instruments Incorporated nor the names of
    its contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 ******************************************************************************
   Release Name: simplelink_cc26x2_sdk_2_30_00_34
   Release Date: 2018-10-04 14:27:27
 *****************************************************************************/

/*********************************************************************
 * INCLUDES
 */
#include <string.h>

//#include <xdc/runtime/Log.h> // Comment this in to use xdc.runtime.Log
#include <ti/common/cc26xx/uartlog/UartLog.h>  // Comment out if using xdc Log

#include <icall.h>

/* This Header file contains all BLE API and icall structure definition */
#include "icall_ble_api.h"
//#include "Event.h"
#include "sensor_service.h"
#include "osal_tasks.h"
#include <stdio.h>
#include "temperature.h"

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * CONSTANTS
 */

/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * GLOBAL VARIABLES
 */

// Sensor_Service Service UUID
CONST uint8_t SensorServiceUUID[ATT_UUID_SIZE] =
{
    SENSOR_SERVICE_SERV_UUID_BASE128(SENSOR_SERVICE_SERV_UUID)
};

// Temp UUID
CONST uint8_t ds_TempUUID[ATT_UUID_SIZE] =
{
    DS_TEMP_UUID_BASE128(DS_TEMP_UUID)
};

// Shock UUID
CONST uint8_t ds_ShockUUID[ATT_UUID_SIZE] =
{
    DS_SHOCK_UUID_BASE128(DS_SHOCK_UUID)
};

/*********************************************************************
 * LOCAL VARIABLES
 */

static SensorServiceCBs_t *pAppCBs = NULL;
static uint8_t ds_icall_rsp_task_id = INVALID_TASK_ID;

#define NUM_TEMP_READINGS 400
uint16_t temp_readings[NUM_TEMP_READINGS];
uint16_t temp_read_index = 0;
uint16_t temp_write_index = 0;
#define MSG_LEN 50
char msg_to_send[MSG_LEN];
volatile uint8_t timer_status;
volatile uint16_t tempC = 0;

uint32 EH_timestamp = 0;
uint16 EH_timegap = 0;
uint16 EH_DemodTimegap = 0;
uint8 EH_State = 0;
uint16 EH_PrevMsg = 0xfff0;
uint16 EH_LastLocationSent = 0xfff1;
bool EH_LocationChanged = false;
uint16 EH_previousLocationSentToAP = 0xff00;
bool EH_IN_VIEW_Timer_Running = false;
uint16_t EH_DemodMsg = 0xfff2;

/*********************************************************************
 * Profile Attributes - variables
 */

// Service declaration
static CONST gattAttrType_t SensorServiceDecl = { ATT_UUID_SIZE, SensorServiceUUID };

// Characteristic "Temp" Properties (for declaration)
static uint8_t ds_TempProps = GATT_PROP_READ | GATT_PROP_WRITE;

// Characteristic "Temp" Value variable
static uint8_t ds_TempVal[DS_TEMP_LEN] = {0};

// Length of data in characteristic "Temp" Value variable, initialized to minimal size.
static uint16_t ds_TempValLen = DS_TEMP_LEN_MIN;

// Characteristic "Shock" Properties (for declaration)
static uint8_t ds_ShockProps = GATT_PROP_NOTIFY | GATT_PROP_WRITE_NO_RSP;

// Characteristic "Shock" Value variable
static uint8_t ds_ShockVal[DS_SHOCK_LEN] = {0};

// Length of data in characteristic "Shock" Value variable, initialized to minimal size.
static uint16_t ds_ShockValLen = DS_SHOCK_LEN_MIN;

// Characteristic "Shock" Client Characteristic Configuration Descriptor
static gattCharCfg_t *ds_ShockConfig;
static uint8 sensor_service_TaskId;

/*********************************************************************
 * Profile Attributes - Table
 */

static gattAttribute_t Sensor_ServiceAttrTbl[] =
{
    // Sensor_Service Service Declaration
    {
        { ATT_BT_UUID_SIZE, primaryServiceUUID },
        GATT_PERMIT_READ,
        0,
        (uint8_t *)&SensorServiceDecl
    },
    // Temp Characteristic Declaration
    {
        { ATT_BT_UUID_SIZE, characterUUID },
        GATT_PERMIT_READ,
        0,
        &ds_TempProps
    },
    // Temp Characteristic Value
    {
        { ATT_UUID_SIZE, ds_TempUUID },
        GATT_PERMIT_READ | GATT_PERMIT_WRITE,
        0,
        ds_TempVal
    },
    // Shock Characteristic Declaration
    {
        { ATT_BT_UUID_SIZE, characterUUID },
        GATT_PERMIT_READ,
        0,
        &ds_ShockProps
    },
    // Shock Characteristic Value
    {
        { ATT_UUID_SIZE, ds_ShockUUID },
        GATT_PERMIT_WRITE,
        0,
        ds_ShockVal
    },
    // Shock CCCD
    {
        { ATT_BT_UUID_SIZE, clientCharCfgUUID },
        GATT_PERMIT_READ | GATT_PERMIT_WRITE,
        0,
        (uint8_t *)&ds_ShockConfig
    },
};

/*********************************************************************
 * LOCAL FUNCTIONS
 */
static bStatus_t Sensor_Service_ReadAttrCB(uint16_t connHandle,
                                         gattAttribute_t *pAttr,
                                         uint8_t *pValue,
                                         uint16_t *pLen,
                                         uint16_t offset,
                                         uint16_t maxLen,
                                         uint8_t method);
static bStatus_t Sensor_Service_WriteAttrCB(uint16_t connHandle,
                                          gattAttribute_t *pAttr,
                                          uint8_t *pValue,
                                          uint16_t len,
                                          uint16_t offset,
                                          uint8_t method);
static uint16_t get_num_entries();

/*********************************************************************
 * PROFILE CALLBACKS
 */
// Simple Profile Service Callbacks
CONST gattServiceCBs_t Sensor_ServiceCBs =
{
    Sensor_Service_ReadAttrCB, // Read callback function pointer
    Sensor_Service_WriteAttrCB, // Write callback function pointer
    NULL                     // Authorization callback function pointer
};

/*********************************************************************
 * PUBLIC FUNCTIONS
 */
extern uint16 sensor_service_ProcessEvent(uint8 taskId, uint16 events )
{
    (void)taskId;  // Intentionally unreferenced parameter
    
    if ( events & SENSOR_POLL_EVT )
    {
        osal_start_timerEx( sensor_service_TaskId, SENSOR_POLL_EVT, SENSOR_POLL_PERIOD );
        if (get_num_entries() < (NUM_TEMP_READINGS-1))
        {
            temp_readings[temp_write_index] = readTemperature();
            temp_write_index++;
            if (temp_write_index > NUM_TEMP_READINGS)
            {
                temp_write_index = 0;
            }
        }
        // Return unprocessed events
        return ( events ^ SENSOR_POLL_EVT );
    }
    return 0;
}

extern void sensor_service_Init(uint8 taskId)
{
    sensor_service_TaskId = taskId;

    EH_timestamp = 0;
    EH_timegap = 0;
    EH_DemodTimegap = 0;
    EH_State = 0;
    EH_PrevMsg = 0xfff0;
    EH_LocationChanged = false;
    EH_DemodMsg = 0xfff2;
}

/*
 * SensorService_AddService- Initializes the SensorService service by registering
 *          GATT attributes with the GATT server.
 *
 *    rspTaskId - The ICall Task Id that should receive responses for Indications.
 */
extern bStatus_t SensorService_AddService(uint8_t rspTaskId)
{
    uint8_t status;

//    sensor_service_TaskId = rspTaskId;

    temp_write_index = 0;
    temp_read_index = 0;

    timer_status = osal_start_timerEx( sensor_service_TaskId, SENSOR_POLL_EVT, SENSOR_POLL_PERIOD );

    // Allocate Client Characteristic Configuration table
    ds_ShockConfig = (gattCharCfg_t *)ICall_malloc(
        sizeof(gattCharCfg_t) * linkDBNumConns);
    if(ds_ShockConfig == NULL)
    {
        return(bleMemAllocError);
    }

    // Initialize Client Characteristic Configuration attributes
    GATTServApp_InitCharCfg(LINKDB_CONNHANDLE_INVALID, ds_ShockConfig);
    // Register GATT attribute list and CBs with GATT Server App
    status = GATTServApp_RegisterService(Sensor_ServiceAttrTbl,
                                         GATT_NUM_ATTRS(Sensor_ServiceAttrTbl),
                                         GATT_MAX_ENCRYPT_KEY_SIZE,
                                         &Sensor_ServiceCBs);
    Log_info1("Registered service, %d attributes",
              GATT_NUM_ATTRS(Sensor_ServiceAttrTbl));
    ds_icall_rsp_task_id = rspTaskId;

    return(status);
}

/*
 * SensorService_RegisterAppCBs - Registers the application callback function.
 *                    Only call this function once.
 *
 *    appCallbacks - pointer to application callbacks.
 */
bStatus_t SensorService_RegisterAppCBs(SensorServiceCBs_t *appCallbacks)
{
    if(appCallbacks)
    {
        pAppCBs = appCallbacks;
        Log_info1("Registered callbacks to application. Struct %p",
                  (uintptr_t)appCallbacks);
        return(SUCCESS);
    }
    else
    {
        Log_warning0("Null pointer given for app callbacks.");
        return(FAILURE);
    }
}

/*
 * SensorService_SetParameter - Set a SensorService parameter.
 *
 *    param - Profile parameter ID
 *    len   - length of data to write
 *    value - pointer to data to write.  This is dependent on
 *            the parameter ID and may be cast to the appropriate
 *            data type (example: data type of uint16_t will be cast to
 *            uint16_t pointer).
 */
bStatus_t SensorService_SetParameter(uint8_t param, uint16_t len, void *value)
{
    bStatus_t ret = SUCCESS;
    uint8_t  *pAttrVal;
    uint16_t *pValLen;
    uint16_t valMinLen;
    uint16_t valMaxLen;
    uint8_t sendNotiInd = FALSE;
    gattCharCfg_t *attrConfig;
    uint8_t needAuth;

    switch(param)
    {
    case DS_TEMP_ID:
        pAttrVal = ds_TempVal;
        pValLen = &ds_TempValLen;
        valMinLen = DS_TEMP_LEN_MIN;
        valMaxLen = DS_TEMP_LEN;
        Log_info2("SetParameter : %s len: %d", (uintptr_t)"Temp", len);
        break;

    case DS_SHOCK_ID:
        pAttrVal = ds_ShockVal;
        pValLen = &ds_ShockValLen;
        valMinLen = DS_SHOCK_LEN_MIN;
        valMaxLen = DS_SHOCK_LEN;
        sendNotiInd = TRUE;
        attrConfig = ds_ShockConfig;
        needAuth = FALSE;  // Change if authenticated link is required for sending.
        Log_info2("SetParameter : %s len: %d", (uintptr_t)"Shock", len);
        break;

    default:
        Log_error1("SetParameter: Parameter #%d not valid.", param);
        return(INVALIDPARAMETER);
    }

    // Check bounds, update value and send notification or indication if possible.
    if(len <= valMaxLen && len >= valMinLen)
    {
        memcpy(pAttrVal, value, len);
        *pValLen = len; // Update length for read and get.

        if(sendNotiInd)
        {
            Log_info2("Trying to send noti/ind: connHandle %x, %s",
                      attrConfig[0].connHandle,
                      (uintptr_t)((attrConfig[0].value ==
                                   0) ? "\x1b[33mNoti/ind disabled\x1b[0m" :
                                  (attrConfig[0].value ==
                                   1) ? "Notification enabled" :
                                  "Indication enabled"));
            // Try to send notification.
            GATTServApp_ProcessCharCfg(attrConfig, pAttrVal, needAuth,
                                       Sensor_ServiceAttrTbl,
                                       GATT_NUM_ATTRS(
                                           Sensor_ServiceAttrTbl),
                                       ds_icall_rsp_task_id,
                                       Sensor_Service_ReadAttrCB);
        }
    }
    else
    {
        Log_error3("Length outside bounds: Len: %d MinLen: %d MaxLen: %d.", len,
                   valMinLen,
                   valMaxLen);
        ret = bleInvalidRange;
    }

    return(ret);
}

/*
 * SensorService_GetParameter - Get a SensorService parameter.
 *
 *    param - Profile parameter ID
 *    len   - pointer to a variable that contains the maximum length that can be written to *value.
              After the call, this value will contain the actual returned length.
 *    value - pointer to data to write.  This is dependent on
 *            the parameter ID and may be cast to the appropriate
 *            data type (example: data type of uint16_t will be cast to
 *            uint16_t pointer).
 */
bStatus_t SensorService_GetParameter(uint8_t param, uint16_t *len, void *value)
{
    bStatus_t ret = SUCCESS;
    switch(param)
    {
    case DS_TEMP_ID:
        *len = MIN(*len, ds_TempValLen);
        memcpy(value, ds_TempVal, *len);
        Log_info2("GetParameter : %s returning %d bytes", (uintptr_t)"Temp",
                  *len);
        break;

    case DS_SHOCK_ID:
        *len = MIN(*len, ds_ShockValLen);
        memcpy(value, ds_ShockVal, *len);
        Log_info2("GetParameter : %s returning %d bytes", (uintptr_t)"Shock",
                  *len);
        break;

    default:
        Log_error1("GetParameter: Parameter #%d not valid.", param);
        ret = INVALIDPARAMETER;
        break;
    }
    return(ret);
}

/*********************************************************************
 * @internal
 * @fn          Sensor_Service_findCharParamId
 *
 * @brief       Find the logical param id of an attribute in the service's attr table.
 *
 *              Works only for Characteristic Value attributes and
 *              Client Characteristic Configuration Descriptor attributes.
 *
 * @param       pAttr - pointer to attribute
 *
 * @return      uint8_t paramID (ref sensor_service.h) or 0xFF if not found.
 */
static uint8_t Sensor_Service_findCharParamId(gattAttribute_t *pAttr)
{
    // Is this a Client Characteristic Configuration Descriptor?
    if(ATT_BT_UUID_SIZE == pAttr->type.len && GATT_CLIENT_CHAR_CFG_UUID ==
       *(uint16_t *)pAttr->type.uuid)
    {
        return(Sensor_Service_findCharParamId(pAttr - 1)); // Assume the value attribute precedes CCCD and recurse
    }
    // Is this attribute in "Temp"?
    else if(ATT_UUID_SIZE == pAttr->type.len &&
            !memcmp(pAttr->type.uuid, ds_TempUUID, pAttr->type.len))
    {
        return(DS_TEMP_ID);
    }
    // Is this attribute in "Shock"?
    else if(ATT_UUID_SIZE == pAttr->type.len &&
            !memcmp(pAttr->type.uuid, ds_ShockUUID, pAttr->type.len))
    {
        return(DS_SHOCK_ID);
    }
    else
    {
        return(0xFF); // Not found. Return invalid.
    }
}

/*********************************************************************
 * @fn          Sensor_Service_ReadAttrCB
 *
 * @brief       Read an attribute.
 *
 * @param       connHandle - connection message was received on
 * @param       pAttr - pointer to attribute
 * @param       pValue - pointer to data to be read
 * @param       pLen - length of data to be read
 * @param       offset - offset of the first octet to be read
 * @param       maxLen - maximum length of data to be read
 * @param       method - type of read message
 *
 * @return      SUCCESS, blePending or Failure
 */
static bStatus_t Sensor_Service_ReadAttrCB(uint16_t connHandle,
                                         gattAttribute_t *pAttr,
                                         uint8_t *pValue, uint16_t *pLen,
                                         uint16_t offset,
                                         uint16_t maxLen,
                                         uint8_t method)
{
    bStatus_t status = SUCCESS;
    uint16_t valueLen;
    uint8_t paramID = 0xFF;

    // Find settings for the characteristic to be read.
    paramID = Sensor_Service_findCharParamId(pAttr);
    switch(paramID)
    {
    case DS_TEMP_ID:
        valueLen = ds_TempValLen;

        Log_info4("ReadAttrCB : %s connHandle: %d offset: %d method: 0x%02x",
                  (uintptr_t)"Temp",
                  connHandle,
                  offset,
                  method);
        /* Other considerations for Temp can be inserted here */
        break;

    case DS_SHOCK_ID:
        valueLen = ds_ShockValLen;

        Log_info4("ReadAttrCB : %s connHandle: %d offset: %d method: 0x%02x",
                  (uintptr_t)"Shock",
                  connHandle,
                  offset,
                  method);
        /* Other considerations for Shock can be inserted here */
        break;

    default:
        Log_error0("Attribute was not found.");
        return(ATT_ERR_ATTR_NOT_FOUND);
    }
    // Check bounds and return the value
    if(offset > valueLen)   // Prevent malicious ATT ReadBlob offsets.
    {
        Log_error0("An invalid offset was requested.");
        status = ATT_ERR_INVALID_OFFSET;
    }
    else
    {
        *pLen = MIN(maxLen, valueLen - offset); // Transmit as much as possible
        memcpy(pValue, pAttr->pValue + offset, *pLen);
    }

    return(status);
}

/*********************************************************************
 * @fn      Sensor_Service_WriteAttrCB
 *
 * @brief   Validate attribute data prior to a write operation
 *
 * @param   connHandle - connection message was received on
 * @param   pAttr - pointer to attribute
 * @param   pValue - pointer to data to be written
 * @param   len - length of data
 * @param   offset - offset of the first octet to be written
 * @param   method - type of write message
 *
 * @return  SUCCESS, blePending or Failure
 */
static bStatus_t Sensor_Service_WriteAttrCB(uint16_t connHandle,
                                          gattAttribute_t *pAttr,
                                          uint8_t *pValue, uint16_t len,
                                          uint16_t offset,
                                          uint8_t method)
{
    bStatus_t status = SUCCESS;
    uint8_t paramID = 0xFF;
    uint8_t changeParamID = 0xFF;
    uint16_t writeLenMin;
    uint16_t writeLenMax;
    uint16_t *pValueLenVar;
    char * pch;
    char msg[MSG_LEN];
    pch = strtok((char *)pValue,",");
    if (strncmp(pch,"Next",4) != 0)
    {
        if (strncmp(pch,"Reset",5) == 0)
        {
            temp_read_index = 0;
            temp_write_index = 0;
        }
        else if (strncmp(pch,"Ack: ",5) == 0)
        {
            if (atoi(&pch[5]) >= 0 && atoi(&pch[5]) <= NUM_TEMP_READINGS)
            {
                temp_read_index = atoi(&pch[5]) + 1;
                if (temp_read_index > NUM_TEMP_READINGS)
                {
                    temp_read_index = 0;
                }
            }
            else
            {
                // invalid remote_msg_index. Reset parameters
                temp_read_index = 0;
                temp_write_index = 0;
            }
        }
    }

    if (temp_write_index == temp_read_index)
    {
        len = 3;
        memcpy(pValue, "End", len);
    }
    else
    {
        if (get_num_entries() < (NUM_TEMP_READINGS-1))
        {
            sprintf(msg, "Seq: %03d, Temp: ",temp_read_index);
            len = 16;
            memcpy(pValue, msg, len);
            sprintf(msg, "%2.1f C",temp_readings[temp_read_index]/2.0);
            memcpy((char *)&pValue[16], msg, 6);
            len += 6;
        }
    }

    if (get_num_entries() > 1) {
        memcpy(&pValue[len], ", Stfwd", 7);
        len += 7;
    }

    // See if request is regarding a Client Characterisic Configuration
    if(ATT_BT_UUID_SIZE == pAttr->type.len && GATT_CLIENT_CHAR_CFG_UUID ==
       *(uint16_t *)pAttr->type.uuid)
    {
        Log_info3("WriteAttrCB (CCCD): param: %d connHandle: %d %s",
                  Sensor_Service_findCharParamId(pAttr),
                  connHandle,
                  (uintptr_t)(method ==
                              GATT_LOCAL_WRITE ? "- restoring bonded state" :
                              "- OTA write"));

        // Allow notification and indication, but do not check if really allowed per CCCD.
        status = GATTServApp_ProcessCCCWriteReq(
            connHandle, pAttr, pValue, len,
            offset,
            GATT_CLIENT_CFG_NOTIFY |
            GATT_CLIENT_CFG_INDICATE);
        if(SUCCESS == status && pAppCBs && pAppCBs->pfnCfgChangeCb)
        {
            pAppCBs->pfnCfgChangeCb(connHandle,
                                    Sensor_Service_findCharParamId(
                                        pAttr), len, pValue);
        }

        return(status);
    }

    // Find settings for the characteristic to be written.
    paramID = Sensor_Service_findCharParamId(pAttr);
    switch(paramID)
    {
    case DS_TEMP_ID:
        writeLenMin = DS_TEMP_LEN_MIN;
        writeLenMax = DS_TEMP_LEN;
        pValueLenVar = &ds_TempValLen;

        Log_info5(
            "WriteAttrCB : %s connHandle(%d) len(%d) offset(%d) method(0x%02x)",
            (uintptr_t)"Temp",
            connHandle,
            len,
            offset,
            method);
        /* Other considerations for Temp can be inserted here */
        break;

    case DS_SHOCK_ID:
        writeLenMin = DS_SHOCK_LEN_MIN;
        writeLenMax = DS_SHOCK_LEN;
        pValueLenVar = &ds_ShockValLen;

        Log_info5(
            "WriteAttrCB : %s connHandle(%d) len(%d) offset(%d) method(0x%02x)",
            (uintptr_t)"Shock",
            connHandle,
            len,
            offset,
            method);
        /* Other considerations for Shock can be inserted here */
        break;

    default:
        Log_error0("Attribute was not found.");
        return(ATT_ERR_ATTR_NOT_FOUND);
    }
    // Check whether the length is within bounds.
    if(offset >= writeLenMax)
    {
        Log_error0("An invalid offset was requested.");
        status = ATT_ERR_INVALID_OFFSET;
    }
    else if(offset + len > writeLenMax)
    {
        Log_error0("Invalid value length was received.");
        status = ATT_ERR_INVALID_VALUE_SIZE;
    }
    else if(offset + len < writeLenMin &&
            (method == ATT_EXECUTE_WRITE_REQ || method == ATT_WRITE_REQ))
    {
        // Refuse writes that are lower than minimum.
        // Note: Cannot determine if a Reliable Write (to several chars) is finished, so those will
        //       only be refused if this attribute is the last in the queue (method is execute).
        //       Otherwise, reliable writes are accepted and parsed piecemeal.
        Log_error0("Invalid value length was received.");
        status = ATT_ERR_INVALID_VALUE_SIZE;
    }
    else
    {
        // Copy pValue into the variable we point to from the attribute table.
        memcpy(pAttr->pValue + offset, pValue, len);

        // Only notify application and update length if enough data is written.
        //
        // Note: If reliable writes are used (meaning several attributes are written to using ATT PrepareWrite),
        //       the application will get a callback for every write with an offset + len larger than _LEN_MIN.
        // Note: For Long Writes (ATT Prepare + Execute towards only one attribute) only one callback will be issued,
        //       because the write fragments are concatenated before being sent here.
        if(offset + len >= writeLenMin)
        {
            changeParamID = paramID;
            *pValueLenVar = offset + len; // Update data length.
        }
    }

    // Let the application know something changed (if it did) by using the
    // callback it registered earlier (if it did).
    if(changeParamID != 0xFF)
    {
        if(pAppCBs && pAppCBs->pfnChangeCb)
        {
            pAppCBs->pfnChangeCb(connHandle, paramID, len + offset, pValue); // Call app function from stack task context.
        }
    }
    return(status);
}

static uint16_t get_num_entries()
{
    uint16_t ret_value = 0;

    if (temp_read_index > temp_write_index)
    {
        ret_value = temp_write_index + NUM_TEMP_READINGS - temp_read_index;
    }
    else
    {
        ret_value = temp_write_index - temp_read_index;
    }
    return ret_value;
}
