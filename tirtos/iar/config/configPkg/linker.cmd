/*
 * Do not modify this file; it is automatically generated from the template
 * linkcmd.xdt in the ti.platforms.simplelink package and will be overwritten.
 */

"C:\ti\simplelink_cc13x2_26x2_sdk_2_40_00_81\examples\rtos\CC26X2R1_LAUNCHXL\ble5stack\project_zero\tirtos\iar\config\configPkg\package\cfg\ble_debug_prm4f.orm4f"
"C:\ti\simplelink_cc13x2_26x2_sdk_2_40_00_81\kernel\tirtos\packages\ti\uia\sysbios\lib\release\ti.uia.sysbios.arm4f"
"C:\ti\simplelink_cc13x2_26x2_sdk_2_40_00_81\kernel\tirtos\packages\ti\uia\loggers\lib\release\ti.uia.loggers.arm4f"
"C:\ti\simplelink_cc13x2_26x2_sdk_2_40_00_81\examples\rtos\CC26X2R1_LAUNCHXL\ble5stack\project_zero\tirtos\src\sysbios\sysbios.arm4f"
"C:\ti\simplelink_cc13x2_26x2_sdk_2_40_00_81\kernel\tirtos\packages\ti\uia\services\lib\release\ti.uia.services.arm4f"
"C:\ti\simplelink_cc13x2_26x2_sdk_2_40_00_81\kernel\tirtos\packages\ti\uia\runtime\lib\release\ti.uia.runtime.arm4f"
"C:\ti\simplelink_cc13x2_26x2_sdk_2_40_00_81\kernel\tirtos\packages\ti\uia\events\lib\release\ti.uia.events.arm4f"
"C:\ti\simplelink_cc13x2_26x2_sdk_2_40_00_81\kernel\tirtos\packages\iar\targets\arm\rts\lib\release\boot.arm4f"


/* Content from xdc.services.global (null): */

/* Content from xdc (null): */

/* Content from xdc.corevers (null): */

/* Content from xdc.shelf (null): */

/* Content from xdc.services.spec (null): */

/* Content from xdc.services.intern.xsr (null): */

/* Content from xdc.services.intern.gen (null): */

/* Content from xdc.services.intern.cmd (null): */

/* Content from xdc.bld (null): */

/* Content from iar.targets.arm (null): */

/* Content from xdc.rov (null): */

/* Content from xdc.runtime (null): */

/* Content from iar.targets.arm.rts (): */

/* Content from ti.sysbios.interfaces (null): */

/* Content from ti.sysbios.family (null): */

/* Content from ti.sysbios.family.arm (ti/sysbios/family/arm/linkcmd.xdt): */

/* Content from xdc.services.getset (null): */

/* Content from xdc.rta (null): */

/* Content from ti.uia.events (null): */

/* Content from ti.uia.runtime (null): */

/* Content from xdc.runtime.knl (null): */

/* Content from ti.sysbios.rts (ti/sysbios/rts/linkcmd.xdt): */

/* Content from ti.uia.services (null): */

/* Content from xdc.platform (null): */

/* Content from ti.catalog.arm.cortexm4 (null): */

/* Content from ti.catalog (null): */

/* Content from ti.catalog.peripherals.hdvicp2 (null): */

/* Content from ti.catalog.arm.peripherals.timers (null): */

/* Content from xdc.cfg (null): */

/* Content from ti.platforms.simplelink (null): */

/* Content from ti.sysbios.hal (null): */

/* Content from ti.sysbios (null): */

/* Content from ti.sysbios.family.arm.cc26xx (null): */

/* Content from ti.sysbios.family.arm.m3 (ti/sysbios/family/arm/m3/linkcmd.xdt): */
--entry __iar_program_start
--keep __vector_table
--define_symbol ti_sysbios_family_arm_m3_Hwi_nvic=0xe000e000

/* Content from ti.sysbios.knl (null): */

/* Content from ti.sysbios.syncs (null): */

/* Content from ti.sysbios.heaps (null): */

/* Content from ti.sysbios.gates (null): */

/* Content from ti.sysbios.xdcruntime (null): */

/* Content from ti.uia.loggers (null): */

/* Content from ti.uia.sysbios (null): */

/* Content from ti.sysbios.utils (null): */

/* Content from configPkg (null): */

/* Content from xdc.services.io (null): */


--define_symbol xdc_runtime_Startup__EXECFXN__C=1
--define_symbol xdc_runtime_Startup__RESETFXN__C=1


--keep __ASM__
--keep __PLAT__
--keep __ISA__
--keep __TARG__
--keep __TRDR__
